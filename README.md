# Help Centre
Help centre source, based on osTicket

## Project Description
Mostly a stock osTicket with some minor changes to re-theme osTicket and offer a more integrated
experience for our users.

## Branding
In the interest of transparency, we include the verbatim source of what we use on our help centre and as such this repository contains the copyrighted logos of Eternal Goth. We kindly ask that you remove our logos before using this source code.

## Thanks!
Big thanks to everyone who has worked on osTicket and all involved!

## License
This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License version 2, as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see http://www.gnu.org/licenses/.
