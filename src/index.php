<?php
/*********************************************************************
    index.php

    Helpdesk landing page. Please customize it to fit your needs.

    Peter Rotich <peter@osticket.com>
    Copyright (c)  2006-2013 osTicket
    http://www.osticket.com

    Released under the GNU General Public License WITHOUT ANY WARRANTY.
    See LICENSE.TXT for details.

    vim: expandtab sw=4 ts=4 sts=4:
**********************************************************************/
require('client.inc.php');

require_once INCLUDE_DIR . 'class.page.php';

$section = 'home';
require(CLIENTINC_DIR.'header.inc.php');
?>
</div>

<div class="container-fluid bgfront">
    <div class="container">
        <div class="row support-image front-boxes">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="intro">
                    <?php
                        if ($cfg && ($page = $cfg->getLandingPage())) {
                            echo $page->getBodyWithImages();
                        } else {
                            echo '<h1>' . __('Quick and Efficient Support') . '</h1>';
                        }
                    ?>
                </div>
            </div></div></div></div>
<div class="container">
    <div class="row front-boxes">
        <div class="col-xs-12 col-sm-12 col-lg-12">
            <div class="row bgcolor">
                <div class="col-xs-12 col-sm-6 col-lg-4">
                    <div class="text-justify fbox">
                        <div class="icon">
                            <div class="image"><i class="fa fa-lightbulb-o"></i></div>
                        </div>
                        <div class="text-box-middle info"><?php echo __('Before creating your ticket, please check our Knowledgebase. Our Knowledgebase includes many of our most asked questions and can provide solutions to most of your queries.') ?></div>
                        <div class="text-right button"><a class="btn btn-info" href="<?php echo ROOT_PATH; ?>kb/index.php"><?php echo __('Knowledgebase') ?></a></div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-6 col-lg-4">
                    <div class="text-justify fbox">
                        <div class="icon">
                            <div class="image"><i class="fa fa-question"></i></div>
                        </div>
                        <div class="text-box-middle info"><?php echo __('Submit a new support request. Please provide as much detail as possible so we can best assist you. To update a previously submitted ticket, please click on the "Check Ticket Status" link. A valid email address is required.') ?></div>
                        <div class="text-right button"><a class="btn btn-success" href="<?php echo ROOT_PATH; ?>open.php"><?php echo __('Open a New Ticket') ?></a></div>
                    </div>
                </div>

                <div class="col-xs-12 col-sm-6 col-lg-4">
                    <div class="text-justify fbox">
                        <div class="icon">
                            <div class="image"><i class="fa fa-comments"></i></div>
                        </div>
                        <div class="text-box-middle info"><?php echo __('Check status of previously opened ticket. we provide archives and history of all your support requests complete with responses.') ?></div>

                        <div class="button"><a class="btn btn-primary" href="<?php echo ROOT_PATH; ?>view.php"><?php echo __('Check Ticket Status') ?></a></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<div class="container">
    <?php require(CLIENTINC_DIR . 'footer.inc.php'); ?>
