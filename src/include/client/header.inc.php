<?php
// Dynamic Titles
if ($faq && is_object($faq) && $faq->getLocalQuestion()) {
    $title = $faq->getLocalQuestion() . ' - ' . $cfg->getTitle();
} elseif ($category && is_object($category) && $category->getFullName()) {
    $title = $category->getFullName() . ' - Knowledge Base - ' . $cfg->getTitle();
} elseif (preg_match("/open.php/", $_SERVER['SCRIPT_NAME'])) {
    $title = "Open a New Ticket" . ' - ' . $cfg->getTitle();
} elseif (preg_match( "/view.php/", $_SERVER['SCRIPT_NAME'])) {
    $title = "Check Ticket Status" . ' - ' . $cfg->getTitle();
} elseif (preg_match("/login.php/", $_SERVER['SCRIPT_NAME'])) {
    $title = "Sign In" . ' - ' . $cfg->getTitle();
} elseif (preg_match( "/tickets.php/", $_SERVER['SCRIPT_NAME'])) {
    $title = "Tickets" . ' - ' . $cfg->getTitle();
} elseif (preg_match( "/profile.php/", $_SERVER['SCRIPT_NAME'])) {
    $title = "Manage Your Profile" . ' - ' . $cfg->getTitle();
} elseif (preg_match("/account.php?do=create/", $_SERVER['SCRIPT_NAME'])) {
    $title = "Account Registration" . ' - ' . $cfg->getTitle();
} elseif (preg_match( "/kb\/index.php/", $_SERVER['SCRIPT_NAME'])) {
    $title = "Knowledgebase" . ' - ' . $cfg->getTitle();
} elseif (preg_match( "/kb\/faq.php?a=search/", $_SERVER['SCRIPT_NAME'])) {
    $title = "Search Results" . ' - ' . $cfg->getTitle();
} else {
    $title=($cfg && is_object($cfg) && $cfg->getTitle())
        ? $cfg->getTitle() : 'osTicket :: '.__('Support Ticket System');
}

$signin_url = ROOT_PATH . "login.php"
    . ($thisclient ? "?e=".urlencode($thisclient->getEmail()) : "");
$signout_url = ROOT_PATH . "logout.php?auth=".$ost->getLinkToken();

header("Content-Type: text/html; charset=UTF-8");
header("X-Frame-Options: SAMEORIGIN");
header("Content-Security-Policy: frame-ancestors ".$cfg->getAllowIframes()."; script-src 'self' 'unsafe-inline' " . PLAUSIBLE_SCRIPT_CSP_DOMAIN . "; object-src 'none'");

if (($lang = Internationalization::getCurrentLanguage())) {
    $langs = array_unique(array($lang, $cfg->getPrimaryLanguage()));
    $langs = Internationalization::rfc1766($langs);
    header("Content-Language: ".implode(', ', $langs));
}
?>
<!DOCTYPE html>
<html<?php
if ($lang
        && ($info = Internationalization::getLanguageInfo($lang))
        && (@$info['direction'] == 'rtl'))
    echo ' dir="rtl" class="rtl"';
if ($lang) {
    echo ' lang="' . $lang . '"';
}
// Dropped IE Support Warning
if (osTicket::is_ie())
    $ost->setWarning(__('osTicket no longer supports Internet Explorer.'));
?>>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title><?php echo Format::htmlchars($title); ?></title>
    <meta name="description" content="Our help centre will help you find the assistance you need quickly and effectively.">
    <meta name="keywords" content="help, support, tickets, goth, eternal, knowledge, base">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<?php echo ASSETS_PATH; ?>css/bootstrap.min.css?v=<?php echo PUBLIC_VERSION; ?>" media="screen"/>
	<link rel="stylesheet" href="<?php echo ROOT_PATH; ?>css/osticket.css?v=<?php echo PUBLIC_VERSION; ?>" media="screen"/>
    <link rel="stylesheet" href="<?php echo ASSETS_PATH; ?>css/print.css?v=<?php echo PUBLIC_VERSION; ?>" media="print"/>
    <link rel="stylesheet" href="<?php echo ROOT_PATH; ?>css/typeahead.css?v=<?php echo PUBLIC_VERSION; ?>"
         media="screen" />
    <link type="text/css" href="<?php echo ROOT_PATH; ?>css/ui-lightness/jquery-ui-1.13.2.custom.min.css?v=<?php echo PUBLIC_VERSION; ?>"
        rel="stylesheet" media="screen" />
       <link rel="stylesheet" href="<?php echo ROOT_PATH ?>css/jquery-ui-timepicker-addon.css?v=<?php echo PUBLIC_VERSION; ?>" media="all"/>
    <link rel="stylesheet" href="<?php echo ROOT_PATH; ?>css/thread.css?v=<?php echo PUBLIC_VERSION; ?>" media="screen"/>
    <link rel="stylesheet" href="<?php echo ROOT_PATH; ?>css/redactor.css?v=<?php echo PUBLIC_VERSION; ?>" media="screen"/>
      <link type="text/css" rel="stylesheet" href="<?php echo ROOT_PATH; ?>assets/gothica/fork-awesome/css/fork-awesome.min.css?v=<?php echo PUBLIC_VERSION; ?>"/>
    <link type="text/css" rel="stylesheet" href="<?php echo ROOT_PATH; ?>css/flags.css?v=<?php echo PUBLIC_VERSION; ?>"/>
    <link type="text/css" rel="stylesheet" href="<?php echo ROOT_PATH; ?>css/rtl.css?v=<?php echo PUBLIC_VERSION; ?>"/>
    <link type="text/css" rel="stylesheet" href="<?php echo ROOT_PATH; ?>css/select2.min.css?v=<?php echo PUBLIC_VERSION; ?>"/>
    <!-- Start favicon(s) -->
    <link rel="icon" type="image/png" href="<?php echo ROOT_PATH; ?>images/icon/favicon_16x16.png?v=<?php echo PUBLIC_VERSION; ?>" sizes="16x16" />
    <link rel="icon" type="image/png" href="<?php echo ROOT_PATH; ?>images/icon/favicon_24x24.png?v=<?php echo PUBLIC_VERSION; ?>" sizes="24x24" />
    <link rel="icon" type="image/png" href="<?php echo ROOT_PATH; ?>images/icon/favicon_32x32.png?v=<?php echo PUBLIC_VERSION; ?>" sizes="32x32" />
    <link rel="icon" type="image/png" href="<?php echo ROOT_PATH; ?>images/icon/favicon_48x48.png?v=<?php echo PUBLIC_VERSION; ?>" sizes="48x48" />
    <link rel="icon" type="image/png" href="<?php echo ROOT_PATH; ?>images/icon/favicon_64x64.png?v=<?php echo PUBLIC_VERSION; ?>" sizes="64x64" />
    <link rel="icon" type="image/png" href="<?php echo ROOT_PATH; ?>images/icon/favicon_96x96.png?v=<?php echo PUBLIC_VERSION; ?>" sizes="96x96" />
    <link rel="apple-touch-icon" href="<?php echo ROOT_PATH; ?>images/icon/favicon_57x57.png?v=<?php echo PUBLIC_VERSION; ?>" sizes="57x57" />
    <link rel="apple-touch-icon" href="<?php echo ROOT_PATH; ?>images/icon/favicon_60x60.png?v=<?php echo PUBLIC_VERSION; ?>" sizes="60x60" />
    <link rel="apple-touch-icon" href="<?php echo ROOT_PATH; ?>images/icon/favicon_72x72.png?v=<?php echo PUBLIC_VERSION; ?>" sizes="72x72" />
    <link rel="apple-touch-icon" href="<?php echo ROOT_PATH; ?>images/icon/favicon_76x76.png?v=<?php echo PUBLIC_VERSION; ?>" sizes="76x76" />
    <link rel="apple-touch-icon" href="<?php echo ROOT_PATH; ?>images/icon/favicon_114x114.png?v=<?php echo PUBLIC_VERSION; ?>" sizes="114x114" />
    <link rel="apple-touch-icon" href="<?php echo ROOT_PATH; ?>images/icon/favicon_120x120.png?v=<?php echo PUBLIC_VERSION; ?>" sizes="120x120" />
    <link rel="apple-touch-icon" href="<?php echo ROOT_PATH; ?>images/icon/favicon_144x144.png?v=<?php echo PUBLIC_VERSION; ?>" sizes="144x144" />
    <link rel="apple-touch-icon" href="<?php echo ROOT_PATH; ?>images/icon/favicon_152x152.png?v=<?php echo PUBLIC_VERSION; ?>" sizes="152x152" />
    <meta name="msapplication-TileImage" content="<?php echo ROOT_PATH; ?>images/icon/mstile_144x144.png?v=<?php echo PUBLIC_VERSION; ?>">
    <!-- End favicon(s) -->
    <script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/jquery-3.7.0.min.js?v=<?php echo PUBLIC_VERSION; ?>"></script>
    <script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/jquery-ui-1.13.2.custom.min.js?v=<?php echo PUBLIC_VERSION; ?>"></script>
    <script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/jquery-ui-timepicker-addon.js?v=<?php echo PUBLIC_VERSION; ?>"></script>
    <script src="<?php echo ROOT_PATH; ?>js/osticket.js?v=<?php echo PUBLIC_VERSION; ?>"></script>
    <script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/filedrop.field.js?v=<?php echo PUBLIC_VERSION; ?>"></script>
    <script src="<?php echo ROOT_PATH; ?>js/bootstrap-typeahead.js?v=<?php echo PUBLIC_VERSION; ?>"></script>
    <script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/redactor.min.js?v=<?php echo PUBLIC_VERSION; ?>"></script>
    <script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/redactor-plugins.js?v=<?php echo PUBLIC_VERSION; ?>"></script>
    <script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/redactor-osticket.js?v=<?php echo PUBLIC_VERSION; ?>"></script>
    <script type="text/javascript" src="<?php echo ROOT_PATH; ?>js/select2.min.js?v=<?php echo PUBLIC_VERSION; ?>"></script>
    <link rel="stylesheet" href="<?php echo ROOT_PATH; ?>assets/gothica/css/gothica.min.css?v=<?php echo PUBLIC_VERSION; ?>" media="screen"/>
     <script type="text/javascript" src="<?php echo ASSETS_PATH; ?>js/bootstrap.min.js?v=<?php echo PUBLIC_VERSION; ?>"></script>
     <script type="text/javascript" src="<?php echo ASSETS_PATH; ?>js/custom.js?v=<?php echo PUBLIC_VERSION; ?>"></script>
     <!-- Self Hosted Privacy Friendly Analytics -->
     <script defer data-domain="<?php echo PLAUSIBLE_DOMAIN; ?>" src="<?php echo PLAUSIBLE_SCRIPT; ?>?v=<?php echo PUBLIC_VERSION; ?>"></script>
     <!-- If you've never heard of Plausible Analytics, duck it - it's a great privacy friendly alternative to G Analytics. After all, the less data sent over to G the better. -->
    <?php
    if($ost && ($headers=$ost->getExtraHeaders())) {
        echo "\n\t".implode("\n\t", $headers)."\n";
    }

    // Offer alternate links for search engines
    // @see https://support.google.com/webmasters/answer/189077?hl=en
    if (($all_langs = Internationalization::getConfiguredSystemLanguages())
        && (count($all_langs) > 1)
    ) {
        $langs = Internationalization::rfc1766(array_keys($all_langs));
        $qs = array();
        parse_str($_SERVER['QUERY_STRING'], $qs);
        foreach ($langs as $L) {
            $qs['lang'] = $L; ?>
        <link rel="alternate" href="//<?php echo $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>?<?php
            echo http_build_query($qs); ?>" hreflang="<?php echo $L; ?>" />
<?php
        } ?>
        <link rel="alternate" href="//<?php echo $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']; ?>"
            hreflang="x-default" />
<?php
    }
    ?>
</head>
<body>
    <div class="container">
        <?php
        if($ost->getError())
            echo sprintf('<div class="error_bar">%s</div>', $ost->getError());
        elseif($ost->getWarning())
            echo sprintf('<div class="warning_bar">%s</div>', $ost->getWarning());
        elseif($ost->getNotice())
            echo sprintf('<div class="notice_bar">%s</div>', $ost->getNotice());
        ?>
    <div class="row logotop">
            <div class="col-sm-6">
            <a id="logo" href="<?php echo ROOT_PATH; ?>index.php"
            title="<?php echo __('Help Centre'); ?>">
                <span class="valign-helper"></span>
                <img src="<?php echo ROOT_PATH; ?>logo.php" width="300" height="73" border=0 alt="<?php
                //echo $ost->getConfig()->getTitle(); ?>">
            </a>
            </div> <div class="col-sm-6">

                <?php
if ($cfg && $cfg->isKnowledgebaseEnabled()) { ?>
<div class="search">
    <form method="get" action="/kb/faq.php">
    <input type="hidden" name="a" value="search"/>
    <input type="text" class="form-control input-sm" name="q" class="search" placeholder="<?php echo __('Search our knowledge base'); ?>"/>
    <button type="submit" class="btn btn-info btn-sm"><?php echo __('Search'); ?></button>
    </form>
</div>

<?php
} ?>

            </div>
        </div>
   </div>
  <nav role="navigation" class="navbar navbar-inverse">

    <div class="container">
        <div class="navbar-header">
            <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
             <!-- <?php if ($cfg->getClientRegistrationMode() == 'public') { ?>
                <label class="welcome-user">Welcome <?php echo __('Guest'); ?> </label><?php
                } ?> -->
        <?php if ($thisclient && is_object($thisclient) && $thisclient->isValid()
                    && !$thisclient->isGuest()) { ?>
        <label class="welcome-user">Welcome <?php echo Format::htmlchars($thisclient->getName()); ?> </label>
                    <?php } else {?>
         <label class="welcome-user">Welcome <?php echo __('Guest'); ?> </label>
                    <?php }?>
        </div>
        <div class="navbar-collapse collapse" id="navbar" aria-expanded="false" style="height: 1px;">
            <ul class="nav navbar-nav navbar-right head-menu">
                <?php
if (($all_langs = Internationalization::getConfiguredSystemLanguages())
    && (count($all_langs) > 1)
) {
    $qs = array();
    parse_str($_SERVER['QUERY_STRING'], $qs);
    foreach ($all_langs as $code=>$info) {
        list($lang, $locale) = explode('_', $code);
        $qs['lang'] = $code;
?>
                        <a class="flag flag-<?php echo strtolower($info['flag'] ?: $locale ?: $lang); ?>"
            href="?<?php echo http_build_query($qs);
            ?>" title="<?php echo Internationalization::getLanguageDescription($code); ?>">&nbsp;</a></li>
<?php }
} ?>
             <?php        if($nav){ ?>
            <?php
           if($nav && ($navs=$nav->getNavLinks()) && is_array($navs)){
                foreach($navs as $name =>$nav) {
                    echo sprintf('<li><a class="%s %s" href="%s">%s</a></li>%s',$nav['active']?'active':'',$name,(ROOT_PATH.$nav['href']),$nav['desc'],"\n");
                }
            } ?>

        <?php
        } ?>
             <?php
                if ($thisclient && is_object($thisclient) && $thisclient->isValid()
                    && !$thisclient->isGuest()) {
                // echo Format::htmlchars($thisclient->getName()).'&nbsp;|';
                 ?>
                <li><a href="<?php echo ROOT_PATH; ?>profile.php"><?php echo __('Profile'); ?></a> </li>
            <!--    <li><a href="<?php echo ROOT_PATH; ?>tickets.php"><?php echo sprintf(__('Tickets <b>(%d)</b>'), $thisclient->getNumTickets()); ?></a> </li> -->
                <li><a href="<?php echo $signout_url; ?>"><?php echo __('Sign Out'); ?></a> </li>
            <?php
            } elseif($nav) {
                if ($thisclient && $thisclient->isValid() && $thisclient->isGuest()) { ?>
                    <li><a href="<?php echo $signout_url; ?>"><?php echo __('Sign Out'); ?></a></li><?php
                }
                elseif ($cfg->getClientRegistrationMode() != 'disabled') { ?>
                     <li><a href="<?php echo $signin_url; ?>"><?php echo __('Sign In'); ?></a></li>
<?php
                }
            } ?>
                            </ul>

        </div><!--/.navbar-collapse -->
    </div>
</nav>
        <div class="container">


         <?php if($errors['err']) { ?>
            <div class="alert alert-danger"><?php echo $errors['err']; ?></div>
         <?php }elseif($msg) { ?>
            <div class="alert alert-info"><?php echo $msg; ?></div>
         <?php }elseif($warn) { ?>
            <div class="alert alert-warning"><?php echo $warn; ?></div>
         <?php } ?>
