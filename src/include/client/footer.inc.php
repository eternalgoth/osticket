    </div>
    <div class="footer-clearspace"></div>
    <footer>
        <div id="footer">
            <div class="container">
                <div class="row">
                    <div class="col-sm-2-4">
                        <div class="storeinfo">
                            <p>
                                <b>Eternal Goth</b><br>
                                11a Church Street<br>
                                Gainsborough<br>
                                Lincolnshire<br>
                                DN21 2JJ<br>
                                United Kingdom
                            </p>
                        </div>
                        <div class="seemore">
                            <a href="https://www.eternalgoth.co.uk/index.php?route=information/contact">View More Information</a>
                        </div>
                    </div>
                    <div class="col-sm-2-4">
                        <h5><i class="fa fa-home"></i> Online Store</h5>
                        <ul class="list-unstyled">
                            <li><a href="https://www.eternalgoth.co.uk/">Store Home</a></li>
                            <li><a href="https://www.eternalgoth.co.uk/index.php?route=account/account">My Account</a></li>
                            <li><a href="https://www.eternalgoth.co.uk/index.php?route=account/wishlist">Wishlist</a></li>
                            <li><a href="https://www.eternalgoth.co.uk/index.php?route=checkout/cart">Basket</a></li>
                            <li><a href="https://www.eternalgoth.co.uk/index.php?route=checkout/checkout">Checkout</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-2-4">
                        <h5><i class="fa fa-comment"></i> Social Media</h5>
                        <ul class="list-unstyled">
                            <li><a href="https://www.facebook.com/EternalGoth" target="_blank">Facebook</a></li>
                            <li><a href="https://twitter.com/EternalGothWeb" target="_blank">Twitter</a></li>
                            <li><a href="https://www.instagram.com/eternal_goth/" target="_blank">Instagram</a></li>
                        </ul>
                    </div>
                </div>
                <hr>
                <p><?php
                            if (date('Y') == "2021") {
                                    $copyyear = date('Y');
                            } else {
                                    $copyyear = "2021 - " . date('Y');
                            }
                        ?>
                        &copy; Copyright <?php echo Format::htmlchars((string) $ost->company ?: 'osTicket.com'); ?> <?php echo $copyyear; ?>. <?php echo __('All rights reserved.'); ?></p>
            </div>
        </div>
    </footer>
    <!-- Proudly Powered by Free Open Source Software! -->
<div id="overlay"></div>
<div id="loading">
    <h4><?php echo __('Please Wait!');?></h4>
    <p><?php echo __('Please wait... it will take a second!');?></p>
</div>
<?php
if (($lang = Internationalization::getCurrentLanguage()) && $lang != 'en_US') { ?>
    <script type="text/javascript" src="<?php echo ROOT_PATH; ?>ajax.php/i18n/<?php
        echo $lang; ?>/js"></script>
<?php } ?>
<script type="text/javascript">
    getConfig().resolve(<?php
        include INCLUDE_DIR . 'ajax.config.php';
        $api = new ConfigAjaxAPI();
        print $api->client(false);
    ?>);
</script>
</body>
</html>
